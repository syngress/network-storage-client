# Network Share Client v1.0
![ReactIMG](https://szymonkrajewski.pl/wp-content/uploads/2017/10/012_logo_reactstrap-1.png)

Things you may want to cover:

# SOFTWARE

* NodeJS
* Npm or Yarn node package manager
* Network Share API application
--------

# Project assumptions

Network Share Client Application provides UI for Network Share API.
Application allows you to send files through a dedicated form.

# Application configuration

- Alolwed mime type's ['application/zip', 'application/x-rar-compressed']
- Only 1 file can be uploaded at a time
- Max file size: 30000000000 bytes

## Example of use

At the first time when you open the app, you may not be able to see the shared resources.

![Storage1](https://syngress.pl/images/storage_1.png)

Application should query backend during this time, returning the available resources to the select field.

![Storage2](https://syngress.pl/images/storage_2.png)

Choice of storage is mandatory, you will not be able to send a request if you don't choose available resource to upload the file.

![Storage3](https://syngress.pl/images/storage_3.png)

The file upload can be done in two ways. By dragging a file to the indicated field, or by pressing the select file button.

![Storage4](https://syngress.pl/images/storage_4.png)

The file name appearance right next to the button indicates that the application did not return any validation error, and your file is ready to upload to the selected resource.

![Storage5](https://syngress.pl/images/storage_5.png)

Remember ! Correct transfer of the file to the selected resource is signaled by a green modal window with "upload success" message and even if the transfer rate has reached 100% the file is being processed, only green modal window with the message inform end of the upload process to the selected resource.

![Storage6](https://syngress.pl/images/storage_6.png)

Application supports backend API errors and returning their messages.

Backend for this GUI can be found here:
https://bitbucket.org/syngress/netwrok-storage-api/src/master/
