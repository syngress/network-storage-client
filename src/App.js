import React, { Component } from 'react';
import Navigation from './components/Navigation'
import MainMenu from './components/MainMenu'
import UploadForm from './components/UploadForm'

class App extends Component {
  render() {
    return (
      <div className="App">
          <div className="container-fluid">
            <div className="row">
            <Navigation />
            <MainMenu />
              <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                  <h1 className="h2">Upload Form</h1>
                  <div className="btn-toolbar mb-2 mb-md-0">
                  </div>
                </div>
                  <UploadForm />
              </main>
            </div>
          </div>

      </div>
    );
  }
}


export default App;
