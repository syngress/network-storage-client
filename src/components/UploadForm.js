import React, { Component } from 'react';
import axios from 'axios';
import {Progress} from 'reactstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class UploadForm extends Component {
  constructor(props) {
    super(props);
      this.state = {
        selectedFile: null,
        loaded:0,
        networkShares: [],
        selectednetworkShare: "",
        validationError: ""
      };
  }

  componentDidMount() {
    fetch("http://localhost:8000/shares")
      .then((response) => {
        return response.json();
      })
      .then(data => {
        let sharesFromApi = data.data.map(networkShare => { return {value: networkShare.id, display: networkShare.share_name} })
        this.setState({ networkShares: [{value: '', display: 'Select storage'}].concat(sharesFromApi) });
      }).catch(error => {
        console.log(error);
      });
  }

  checkMimeType = (event) => {
    let files = event.target.files
    let err = []
    const types = ['application/zip', 'application/x-rar-compressed']
    for(var x = 0; x<files.length; x++) {
      if (types.every(type => files[x].type !== type)) {
        err[x] = files[x].type+' is not a supported format\n';
      }
    };
    for(var z = 0; z<err.length; z++) {
      toast.error(err[z])
      event.target.value = null
    }
   return true;
  }

  maxSelectFile = (event) => {
    let files = event.target.files
    if (files.length > 1) {
      const msg = 'Only 1 file can be uploaded at a time'
      event.target.value = null
      toast.warn(msg)
      return false;
    }
    return true;
  }

  checkFileSize = (event) => {
    let files = event.target.files
    let size = 30000000000
    let err = [];
    for(var x = 0; x<files.length; x++) {
      if (files[x].size > size) {
       err[x] = files[x].type + 'is too large\n';
      }
    };
    for(var z = 0; z<err.length; z++) {
      toast.error(err[z])
      event.target.value = null
    }
    return true;
  }

  onChangeHandler = event => {
    var files = event.target.files
    if(this.maxSelectFile(event) && this.checkMimeType(event) && this.checkFileSize(event)) {
      this.setState({
        selectedFile: files,
        loaded:0
      })
    }
  }

  onClickHandler = () => {
    const data = new FormData()
    var selectedNetworkShare = this.state.selectednetworkShare
    if (!isNaN(selectedNetworkShare) && selectedNetworkShare <= 0) {
      let err = [];
      var sns = null;
      err[sns] = 'Select network sahre.\n';
      toast.error(err[sns])
      return;
    }
    if (this.state.selectedFile == null) {
      let err = [];
      var nfs = null;
      err[nfs] = 'No file selected.\n';
      toast.error(err[nfs])
      return;
    }
    for(var x = 0; x<this.state.selectedFile.length; x++) {
      data.append('file', this.state.selectedFile[x])
      data.append('file_name', this.state.selectedFile[x].name.split('.').slice(0, -1).join('.'))
      data.append('file_size', this.state.selectedFile[x].size)
      data.append('extension', getExt(this.state.selectedFile[x].name))
      data.append('encrypted', true)
    }

    function getExt(filename) {
      var idx = filename.lastIndexOf('.');
      return (idx < 1) ? "" : filename.substr(idx + 1);
    }

    axios.post("http://localhost:8000/shares/"+ selectedNetworkShare +"/uploads", data, {
      onUploadProgress: ProgressEvent => {
        this.setState({
          loaded: (ProgressEvent.loaded / ProgressEvent.total*100),
        })
      },
    })
      .then(res => {
        toast.success('upload success')
      })
      .catch(err => {
        var response_error = JSON.stringify(err.response.data.details);
        if (response_error) {
          response_error = response_error.slice(1, response_error.length-1).replace(/"/g, "")
        }
        toast.error(err.message + '\n' + response_error)
      })
    }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="offset-md-3 col-md-6">
            <div className="form-group files">
              <label></label>
              <input type="file" className="form-control" multiple onChange={this.onChangeHandler}/>
            </div>
            <div>
            <label>Select storage</label>
            <div>
              <select className="form-control" value={this.state.selectednetworkShare} onChange = {
                (e) => this.setState({
                  selectednetworkShare: e.target.value,
                  validationError: e.target.value === "" ? "You must select storage" : ""
                })
              }>
              {this.state.networkShares.map((networkShare) => <option key={networkShare.value} value={networkShare.value}>{networkShare.display}</option>)}
              </select>
              <div style={{color: 'red', marginTop: '5px', marginBottom: '10px'}}>
                {this.state.validationError}
              </div>
            </div>
            </div>
            <div className="form-group">
              <ToastContainer />
              <Progress max="100" color="success" value={this.state.loaded} >{Math.round(this.state.loaded,2) }%</Progress>
            </div>
            <button type="button" className="btn btn-success btn-block" onClick={this.onClickHandler}>Upload</button>
          </div>
        </div>
      </div>
    );
  }
}

export default UploadForm
